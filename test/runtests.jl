#!/usr/bin/env julia

using Distributed
addprocs()
println("using $(nprocs()) processors")
using Test
@everywhere using SharedArrays
@everywhere using Fibo

function getrangefromdescr()
    eval(Meta.parse(Test.get_testset().description))
end

@testset "39:45" begin
    range = getrangefromdescr()
    expected = [63245986, 102334155, 165580141, 267914296, 433494437, 701408733, 1134903170]
    print("fib      ")
    @time @test [fib(i) for i in range] == expected
    print("para_fib ")
    @time @test para_fib.(range) == expected
    print("pmap_fib ")
    @time @test pmap(i -> fib(i), range) == expected

    result = SharedArray{Int, 1}((length(range)))
    start = range.start - 1
    print("dist_fib ")
    @time @sync @distributed for i = range result[i - start] = fib(i) end
    @test result == expected

    print("iter_fib ")
    @time @test map(iter_fib, range) == expected
    print("memo_fib ")
    @time @test [memo_fib(i) for i in range] == expected
end

@testset "439:445" begin
    range = getrangefromdescr()
    expected = BigInt[24893677995851695395061591712608896875850555449371181438711037372178370103971256950553836461,
                      40278817102283407038585813270091176624224846310456696876645445975772848265708967220435913205,
                      65172495098135102433647404982700073500075401759827878315356483347951218369680224170989749666,
                      105451312200418509472233218252791250124300248070284575192001929323724066635389191391425662871,
                      170623807298553611905880623235491323624375649830112453507358412671675285005069415562415412537,
                      276075119498972121378113841488282573748675897900397028699360341995399351640458606953841075408,
                      446698926797525733283994464723773897373051547730509482206718754667074636645528022516256487945]
    print("iter_fib ")
    @time @test [iter_fib(i) for i in range] == expected
    print("memo_fib ")
    @time @test [memo_fib(i) for i in range] == expected
end
