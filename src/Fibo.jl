module Fibo
using Distributed

function fib(n)
    if n < 2
        n
    else
        fib(n-2) + fib(n-1)
    end
end

function para_fib(n)
    if n < 40
        fib(n)
    else
        @fetch(para_fib(n-2)) + para_fib(n-1)
    end
end

function iter_fib(n)
    if n < 2
        BigInt(n)
    else
        (a, b) = (BigInt(0), BigInt(1))
        for i in 1:n-1
            (a, b) = (b, a+b)
        end
        return b
    end
end

memo = Dict{Int,BigInt}(0 => 0, 1 => 1)

function memo_fib(n)
    get!(memo, n) do
        memo_fib(n-2) + memo_fib(n-1)
    end
end

export fib, para_fib, iter_fib, memo_fib

end
